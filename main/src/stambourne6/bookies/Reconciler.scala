package stambourne6.bookies

import stambourne6.bookies.sources.Source

// TODO: replace spread with actual relative profit
case class Arbitrage(source1: Source, source2: Source, sourceTwoBetMultiplier: Double, spread: Double)

class Reconciler(sources: Traversable[Source]) {
  private def findArbitrage(allOdds: Traversable[GameWithOdds]): Option[Arbitrage] = {

    val oddsForGames = allOdds.map(odds => odds.game).toSet.size
    require(oddsForGames == 1, s"Supplied odds were for multiple games. Expected odds for a single game.")

    // Computes arbitrage between 2 sources (if one exists)
    def arbitrageForPair(oddsA: GameWithOdds, oddsB: GameWithOdds): Option[Arbitrage] = {
      List((oddsA, oddsB), (oddsB, oddsA))
        .map { case (oddsUsedForBettingOnPlayer1, oddsUsedForBettingOnPlayer2) =>
          val larger = (oddsUsedForBettingOnPlayer1.playerOneOdds - 1) / 2
          val smaller = 2 / (oddsUsedForBettingOnPlayer2.playerTwoOdds - 1)
          if (larger > smaller) {
            //TODO: Need to optimize sourceTwoBetMultiplier for maximum profit
            Some(Arbitrage(
              oddsUsedForBettingOnPlayer1.source,
              oddsUsedForBettingOnPlayer2.source,
              (smaller + larger) / 2,
              (larger - smaller) / (larger + smaller)))
          }
          else {
            None
          }
        }
        .collectFirst { case Some(k) => k }
    }

    allOdds.toSet.subsets(2)
      .map(_.toList).map { case List(oddsA, oddsB) => arbitrageForPair(oddsA, oddsB) }
      .collectFirst { case Some(arbitrage) => arbitrage }
  }

  private val gamesWithMultipleSources = sources
    .flatMap(src => src.allGames())
    .groupBy { gameWithOdds => gameWithOdds.game }
    .filter { case (game, odds) => odds.size > 1 }

  private val availableArbitrages = gamesWithMultipleSources
    .flatMap { case (game, odds) => findArbitrage(odds) }

  def arbitrages() = availableArbitrages
}
