package stambourne6.bookies.sources

import java.time.LocalDateTime

import stambourne6.bookies.{Game, GameType, GameWithOdds}

import scala.xml.XML

object BetClic extends Source {
  override def allGames(): Iterable[GameWithOdds] = {

    val xml = XML.load("http://xml.cdn.betclic.com/odds_en.xml")
    val sports = xml \ "sport"

    val tennisMatches = sports.
      filter(sport => (sport \@ "name") == "Tennis")
      .flatMap(sport => sport \\ "match")
      .filter(tennisMatch => (tennisMatch \@ "name").contains(" - "))
      .map(oneOnOneMatch =>
        (oneOnOneMatch \@ "name",
         LocalDateTime.parse(oneOnOneMatch \@ "start_date"),
         (oneOnOneMatch \@ "name").split(" - "),
         oneOnOneMatch \\ "bet"))
      .flatMap{ case (matchName, date, playerNames, bets) =>
        bets
          .filter(bet => bet \@ "name" == "Match Winner")
          .map(bet => (matchName, date, playerNames, bet))
      }

    tennisMatches.map{case (matchName, date, Array(playerName1, playerName2), bet) =>
      val odds = bet \ "choice"
      GameWithOdds(
        Game(GameType.Tennis, date.toLocalDate, playerName1, playerName2),
        (odds(0) \@ "odd").toDouble,
        (odds(1) \@ "odd").toDouble,
        this,
        "")
    }
  }
}
