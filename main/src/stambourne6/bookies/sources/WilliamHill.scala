package stambourne6.bookies.sources

import java.net.URL
import java.time.LocalDate

import stambourne6.bookies.{Game, GameType, GameWithOdds}

import scala.xml.XML

object WilliamHill extends Source {
  private def marketToURL(marketId: Int) = {
    new URL(s"http://cachepricefeeds.williamhill.com/openbet_cdn?action=template&template=getHierarchyByMarketType&classId=$marketId&marketSort=HH&filterBIR=N")
  }

  // There is a Men's tennis market, Women's tennis market and Challenger's Tennis
  private val tennisMarkets = Set(308, 43, 424)

  override def allGames(): Iterable[GameWithOdds] = {
    val dateParticipantUrlTriplets = tennisMarkets
      .map(marketId => XML.load(marketToURL(marketId)))
      .flatMap(xmlDoc => xmlDoc \ "response" \ "williamhill" \ "class" \ "type" \ "market")
      // Only leave bets on X wins or X loses
      .filter(market => (market \@ "name").endsWith(" - Match Betting"))
      .map(market => (LocalDate.parse(market \@ "date"), market \ "participant", market \@ "url"))

    val gamesWithWeirdNumberOfPlayers = dateParticipantUrlTriplets.filter{ case (_, participants, _) => participants.length != 2 }
    if (gamesWithWeirdNumberOfPlayers.nonEmpty) {
      println(s"Found ${gamesWithWeirdNumberOfPlayers.size} games with an unexpected number of players. E.g. ${gamesWithWeirdNumberOfPlayers.take(5)}")
    }

    dateParticipantUrlTriplets
      .filter{ case (_, participants, _) => participants.size == 2}
      .map{ case (date, participants, url) =>
        GameWithOdds(
          Game(
            GameType.Tennis,
            date,
            participants.theSeq.head \@ "name",
            participants.theSeq(1) \@ "name"),
          (participants.theSeq.head \@ "oddsDecimal").toDouble,
          (participants.theSeq(1) \@ "oddsDecimal").toDouble,
          this,
          url)}
      // For now, exclude 2 v 2 matches because we don't know how to properly model these
      // TODO: address this issue
      .filter{ gameWithOdds => !((gameWithOdds.game.player1 contains "/") || (gameWithOdds.game.player2 contains "/")) }
  }
}
