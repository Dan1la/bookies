package stambourne6.bookies.sources

import stambourne6.bookies.GameWithOdds

trait Source {
  def allGames(): Iterable[GameWithOdds]
}