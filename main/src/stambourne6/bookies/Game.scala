package stambourne6.bookies

import java.time.LocalDate

object GameType extends Enumeration {
  val Tennis = Value
}

case class Game(gameType: GameType.Value,
                gameDate: LocalDate,
                player1: String,
                player2: String) {
  override def toString: String = {
    s"$player1 vs $player2, playing $gameType on $gameDate"
  }
}