package stambourne6.bookies

import stambourne6.bookies.sources.Source

case class GameWithOdds(game: Game, playerOneOdds: Double, playerTwoOdds: Double, source: Source, oddsPage: String)